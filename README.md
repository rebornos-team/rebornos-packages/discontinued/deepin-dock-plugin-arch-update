# deepin-dock-plugin-arch-update

A plugin for deepin dock, Arch Linux update indicator

https://github.com/CareF/deepin-dock-plugin-arch-update

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/rebornos-packages/deepin-dock-plugin-arch-update.git
```

